program Project1;

uses
  Forms,
  rtpMain in 'rtpMain.pas' {rtpForm},
  nal in 'nal.pas',
  rtp in 'rtp.pas',
  ffmpeg in 'ffmpeg.pas',
  rtp_types in 'rtp_types.pas',
  rtspclient in 'rtspclient.pas',
  rtsppacket in 'rtsppacket.pas',
  H264 in 'H264.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TrtpForm, rtpForm);
  Application.Run;
end.
