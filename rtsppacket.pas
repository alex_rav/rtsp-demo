unit rtsppacket;

interface

uses SysUtils, Classes;

type
  Trtsppacket = class
    raw : TStringList;
  public
    constructor create(data:string);
    destructor destroy; override;
    function get(name:string):string;
  private
  end;

implementation

constructor Trtsppacket.create(data:string);
begin
  raw := TStringList.Create;
  data := StringReplace(data, ': ', '=', [rfReplaceAll]);
  data := StringReplace(data, ';', #13#10, [rfReplaceAll]);
  raw.Text := data;
end;

destructor Trtsppacket.destroy;
begin
  FreeAndNil(raw);
end;

function Trtsppacket.get(name:string):string;
var
  i:Integer;
begin
  Result := '';
  if raw.Values[name] > '' then
    Result := raw.Values[name]
  else
    for i:=0 to raw.Count-1 do
      if Pos(name+'=', raw.Strings[i]) <> 0 then
      begin
        Result := Copy(raw.Strings[i], Pos('=', raw.Strings[i])+1, Length(raw.Strings[i])-Pos('=', raw.Strings[i]));
        Break;
      end;
end;

end.
