unit rtspclient;

interface

uses Forms, SysUtils, ScktComp, StdCtrls, jpeg, IdBaseComponent, IdCoder, IdCoder3to4,
  IdCoderMIME, ExtCtrls, IdComponent, IdUDPBase, IdUDPServer,
  IdGlobal, IdSocketHandle, Buttons, IdTCPConnection, IdTCPClient,
  rtp_types, rtp, Variants;

const
  userAgent = 'Lib-RTSP 0.5';
  RTP_HEADER_SIZE = 12;
  NON_IDR_PICTURE: array[0..3] of byte = ($00, $00, $00, $01);
  IDR_SLICE: array[0..0] of Byte = ($65);

type
  TRtcpReadEvent = procedure (Response: string) of object;
  TRtpReadEvent = procedure (Adata: TData) of object;
  TRtpFrameEvent = procedure (raw: TData) of object;

  TRtspClient = class
  private
    fRtcpReadEnd: TRtcpReadEvent;
    fRtpRead: TRtpReadEvent;
    fRtpFrame: TRtpFrameEvent;
    wait: Boolean;
    cseq: Integer;
    ip: string;
    port: Integer;
    ClientSocket: TClientSocket;
    IdUDPServer: TIdUDPServer;
    seq:Integer;
    session: string;
    sps:TData;
    pps:TData;
    sei:TData;
    idr:TData;
    raw:TData;
    frameReady:Boolean;
    procedure ClientSocket1Read(Sender: TObject; Socket: TCustomWinSocket);
    procedure IdUDPServer1UDPRead(AThread: TIdUDPListenerThread; AData: TIdBytes; ABinding: TIdSocketHandle);
    function decode(text:string):string;
    procedure waiting;
    function toData(s:string):TData;

    function GetOnRtcpReadEnd:TRtcpReadEvent;
    procedure SetOnRtcpReadEnd(event:TRtcpReadEvent);

    function GetOnRtpReadEnd:TRtpFrameEvent;
    procedure SetOnRtpReadEnd(event:TRtpFrameEvent);

    function GetOnRtpRead:TRtpReadEvent;
    procedure SetOnRtpRead(event:TRtpReadEvent);

//    function intToHex(Value: integer): string;
  public
    oneFrame:Boolean;
    frame: TRTP;
    UserName: string;
    Password: string;
    Stream: Integer;
    Channel: Integer;
    TrackID: Integer;
    response: string;
    constructor create(ip:string; port:Integer);
    destructor destroy(); override;
    procedure open();
    procedure close();
    function active():Boolean;
    procedure send(command:string);
    property OnRtcpReadEnd: TRtcpReadEvent read GetOnRtcpReadEnd write SetOnRtcpReadEnd;
    property OnRtpRead: TRtpReadEvent read GetOnRtpRead write SetOnRtpRead;
    property OnRtpFrame: TRtpFrameEvent read GetOnRtpReadEnd write SetOnRtpReadEnd;
    property Rtp: TIdUDPServer read IdUDPServer;
    function getRaw():TData;
    function getSps():TData;
    function getPps():TData;
    function getSei():TData;
    function getIdr():TData;
    function getFrame:TData;

  end;

implementation

uses
  rtsppacket;

constructor TRtspClient.create(ip:string; port:Integer);
begin
  cseq := 1;
  ClientSocket := TClientSocket.Create(nil);
  ClientSocket.Host := ip;
  ClientSocket.Port := port;
  ClientSocket.OnRead := ClientSocket1Read;
  Self.ip := ip;
  Self.port := port;
  IdUDPServer := TIdUDPServer.Create;
  IdUDPServer.OnUDPRead := IdUDPServer1UDPRead;
end;

destructor TRtspClient.destroy();
begin
  ClientSocket.Free;
  rtp.Free;
end;

function TRtspClient.getRaw():TData;
begin
  Result := raw;
end;

function TRtspClient.getSps():TData;
begin
  Result := sps;
end;

function TRtspClient.getPps():TData;
begin
  Result := pps;
end;

function TRtspClient.getSei():TData;
begin
  Result := sei;
end;

function TRtspClient.getIdr():TData;
begin
  Result := idr;
end;

function TRtspClient.GetOnRtcpReadEnd:TRtcpReadEvent;
begin
  Result := fRtcpReadEnd;
end;

procedure TRtspClient.SetOnRtcpReadEnd(event:TRtcpReadEvent);
begin
  fRtcpReadEnd := event;
end;

function TRtspClient.GetOnRtpReadEnd:TRtpFrameEvent;
begin
  Result := fRtpFrame;
end;

procedure TRtspClient.SetOnRtpReadEnd(event:TRtpFrameEvent);
begin
  fRtpFrame := event;
end;

function TRtspClient.GetOnRtpRead:TRtpReadEvent;
begin
  Result := fRtpRead;
end;

procedure TRtspClient.SetOnRtpRead(event:TRtpReadEvent);
begin
  fRtpRead := event;
end;

procedure TRtspClient.open();
begin
  ClientSocket.open;
end;

procedure TRtspClient.close();
begin
  ClientSocket.close;
  IdUDPServer.Active := false;
end;

function TRtspClient.active:Boolean;
begin
  Result := ClientSocket.Active;
end;

function TRtspClient.decode(text:string):string;
var
  decoder: TIdDecoderMIME;
begin
  decoder := TIdDecoderMIME.Create(nil);
  try
    result := Decoder.DecodeString(Text);
  finally
    FreeAndNil(Decoder);
  end
end;

function TRtspClient.toData(s:string):TData;
var
  i: Integer;
  tmp: TData;
begin
  SetLength(tmp, Length(s));
  for i:=1 to Length(s) do
    tmp[i-1] := ord(s[i]);
  Result := tmp;
end;

procedure TRtspClient.ClientSocket1Read(Sender: TObject; Socket: TCustomWinSocket);
var
  s:string;
  pkg:Trtsppacket;
  data: string;
  ps : Integer;
begin
  s:=socket.ReceiveText;
  response := s;
  if pos('RTSP/1.0 200 OK', s) > 0 then
    try
      pkg := Trtsppacket.create(s);
      if pkg.get('session') > '' then
        session := pkg.get('session');
      if pkg.get('sprop-parameter-sets') > '' then  // sps-pps in RTSP
      begin
        data := pkg.get('sprop-parameter-sets');
        ps := pos(',', data);
        sps := toData(decode(copy(data,1, ps-1)));
        pps := toData(decode(Copy(data, ps+1, Length(data))));
      end;
    finally
      FreeAndNil(pkg);
    end;
  wait := false;
  if Addr(fRtcpReadEnd) <> nil then
    fRtcpReadEnd(s);
end;
{
function TRtspClient.intToHex(Value: integer): string;
var
  stb, mlb: integer;
const
  hex = '0123456789ABCDEF';
begin
  stb := Value div 16;
  mlb := Value - 16 * stb;
  result := hex[stb + 1] + hex[mlb + 1];
end;
}
procedure addBytes(src:TData; var dest:TData); overload;
var
  i: Integer;
  start: Integer;
begin
  start := Length(dest);
  SetLength(dest, Length(dest)+length(src));
  for i:=0 to Length(src)-1 do
    dest[start+i] := src[i];
end;

procedure addBytes(src:array of byte; var dest:TData); overload;
var
  i: Integer;
  start: Integer;
begin
  start := Length(dest);
  SetLength(dest, Length(dest)+length(src));
  for i:=0 to Length(src)-1 do
    dest[start+i] := src[i];
end;

//////// byte stream /////////////
//NON_IDR_PICTURE +
//SPS +
//NON_IDR_PICTURE +
//PPS +
//NON_IDR_PICTURE +
//SEI +
//NON_IDR_PICTURE +
/////////// raw ////////
//$65 +
//FU-A Start IDR-slice (payload) +
//FU-A (payload)
// ....
//FU-A End IDR (payload)
function TRtspClient.getFrame:TData;
var
  tmp: TData;
begin
  addBytes(NON_IDR_PICTURE, tmp);
  addBytes(sps, tmp);
  addBytes(NON_IDR_PICTURE, tmp);
  addBytes(pps, tmp);
  addBytes(NON_IDR_PICTURE, tmp);
  addBytes(sei, tmp);
  addBytes(NON_IDR_PICTURE, tmp);
  addBytes(IDR_SLICE, tmp);
  addBytes(raw, tmp);
  Result := tmp;
end;

procedure TRtspClient.IdUDPServer1UDPRead(AThread: TIdUDPListenerThread;
  AData: TIdBytes; ABinding: TIdSocketHandle);
var
  tmp: TRTP;
  payLoad: TData;

//function getCC():byte;
//begin
//  Result := byte((tmp.getData[0]) and $0f);
//end;
//
//function getPayloadStart():integer;
//begin
//  result := RTP_HEADER_SIZE + getCC() * 4;
//end;

begin
  if (oneFrame)and(frameReady) then
    Exit;
  tmp := TRTP.create(TData(AData));
  // NAL
  if (tmp.isMarker)and(not tmp.isFu) then
  begin
    seq := tmp.getSequence();
    if tmp.isSPS then
       sps := tmp.getSPS()
    else if tmp.isPPS then
      pps := tmp.getPPS()
    else if tmp.isSEI then
      sei := tmp.getSEI()
    else if tmp.isIdr then
      idr := tmp.getIDR();
  end;
  // FU video data
  if (tmp.isFU)and(tmp.isFuIdr)and(seq+1=tmp.getSequence()) then
  begin
    if ((tmp.isFUFirst)and(length(raw)=0))or((not tmp.isFUFirst)and(Length(raw)>0)) then
    begin
      payLoad := tmp.getPayLoad;
      // add data
      addBytes(payLoad, raw);
    end;
    seq := tmp.getSequence();
  end;
  // full data to decode
  if (tmp.isFU)and(tmp.isFUEnd)and(Length(raw)>20000) then //and(seq=tmp.getSequence()) then
  begin
    frameReady := true;
    if Addr(fRtpFrame) <> nil then
      fRtpFrame(getFrame);
  end;
  if Addr(fRtpRead) <> nil then
    fRtpRead(TData(AData));
  FreeAndNil(tmp);
end;

procedure TRtspClient.waiting;
var
  i:integer;
begin
  i:=10;
  while (wait) and (i>0) do
  begin
    sleep(100);
    application.processmessages;
    i:=i-1;
  end;
  wait := false;
end;

procedure TRtspClient.send(command:string);
var
  tmp: string;
begin
  if command = 'OPTIONS' then
    tmp := 'OPTIONS rtsp://'+ip+'/user='+userName+'&password='+password+'&channel='+inttostr(channel)+'&stream='+inttostr(stream)+'.sdp RTSP/1.0'#13#10'CSeq: '+inttostr(cseq)+#13#10'User-Agent: '+userAgent+#13#10#13#10
  else if command = 'DESCRIBE' then
    tmp := 'DESCRIBE rtsp://'+ip+'/user='+userName+'&password='+password+'&channel='+inttostr(Channel)+'&stream='+inttostr(Stream)+'.sdp RTSP/1.0'#13#10'CSeq: '+inttostr(cseq)+#13#10'User-Agent: '+userAgent+#13#10'Accept: application/sdp'#13#10#13#10
  else if command = 'SETUP' then
    tmp := 'SETUP rtsp://'+ip+'/user='+userName+'&password='+password+'&channel='+inttostr(Channel)+'&stream='+inttostr(Stream)+'.sdp/trackID='+inttostr(trackID)+' RTSP/1.0'#13#10'CSeq: '+inttostr(cseq)+#13#10'User-Agent: '+userAgent+#13#10'Transport: RTP/AVP;unicast;client_port=54700-54701'#13#10#13#10
  else if command = 'PLAY' then
    tmp := 'PLAY rtsp://'+ip+'/user='+userName+'&password='+password+'&channel='+inttostr(Channel)+'&stream='+inttostr(Stream)+'.sdp RTSP/1.0'#13#10'CSeq: '+inttostr(cseq)+#13#10'User-Agent: '+userAgent+#13#10'Session: '+session+#13#10'Range: npt=0.000-'#13#10#13#10
  else if command = 'GET_PARAMETER' then
    tmp := 'GET_PARAMETER rtsp://'+ip+'/user='+userName+'&password='+password+'&channel='+inttostr(Channel)+'&stream='+inttostr(Stream)+'.sdp RTSP/1.0'#13#10'CSeq: '+inttostr(cseq)+#13#10'User-Agent: '+userAgent+#13#10'Session: '+session+#13#10#13#10
  else if command = 'TEARDOWN' then
    tmp := 'TEARDOWN rtsp://172.22.2.9/user='+userName+'&password='+password+'&channel='+inttostr(Channel)+'&stream='+inttostr(Stream)+'.sdp RTSP/1.0'#13#10'CSeq: '+inttostr(cseq)+#13#10'User-Agent: '+userAgent+#13#10'Session: '+session+#13#10#13#10;

  Wait := True;
  ClientSocket.Socket.SendText(tmp);
  cseq := cseq + 1;
  waiting;
end;
end.
