unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ScktComp, StdCtrls, jpeg, IdBaseComponent, IdCoder, IdCoder3to4,
  IdCoderMIME, ExtCtrls, IdComponent, IdUDPBase, IdUDPServer,
  IdGlobal, IdSocketHandle, Buttons, IdTCPConnection, IdTCPClient,
  dxGDIPlusClasses;

type
  TForm1 = class(TForm)
    ConnectButton: TButton;
    StartButton: TButton;
    ClientSocket1: TClientSocket;
    Edit1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Edit2: TEdit;
    Image1: TImage;
    Memo1: TMemo;
    IdUDPServer1: TIdUDPServer;
    StopButton: TButton;
    procedure ConnectButtonClick(Sender: TObject);
    procedure StartButtonClick(Sender: TObject);
    procedure ClientSocket1Read(Sender: TObject; Socket: TCustomWinSocket);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure IdUDPServer1UDPRead(AThread: TIdUDPListenerThread;
      AData: TIdBytes; ABinding: TIdSocketHandle);
    procedure StopButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure waiting;
  public
    { Public declarations }
  end;

  Tpkg = class
    raw : TStringList;
  public
    constructor create(data:string);
    destructor destroy; override;
    function get(name:string):string;
  private
  end;

var
  Form1: TForm1;

implementation

uses
  rtppaket;

var
  mem:tmemorystream;
  jpeg:tjpegimage;
  f:Boolean;
  s:string;
  wait:Boolean;
  seq:Integer;
  //
  session: string;
  sps:string;
  pps:string;
  raw:array of Byte;
  RTPpacket: TRTPpacket;

{$R *.dfm}

constructor Tpkg.create(data:string);
begin
  raw := TStringList.Create;
  data := StringReplace(data, ': ', '=', [rfReplaceAll]);
  data := StringReplace(data, ';', #13#10, [rfReplaceAll]);
  raw.Text := data;
end;
destructor Tpkg.destroy;
begin
  FreeAndNil(raw);
end;
function Tpkg.get(name:string):string;
var
  i:Integer;
begin
  Result := '';
  if raw.Values[name] > '' then
    Result := raw.Values[name]
  else
    for i:=0 to raw.Count-1 do
      if Pos(name+'=', raw.Strings[i]) <> 0 then
      begin
        Result := Copy(raw.Strings[i], Pos('=', raw.Strings[i])+1, Length(raw.Strings[i])-Pos('=', raw.Strings[i]));
        Break;
      end;
end;

procedure log(text:string);
begin
  Form1.memo1.lines.add(text);
end;

procedure TForm1.ConnectButtonClick(Sender: TObject);
begin
  ClientSocket1.Host:=Edit1.Text; //���������� ��� ����� (������ ������)
  ClientSocket1.Port := StrToInt(Edit2.text);
  ClientSocket1.Open; //�����������
  mem:=tmemorystream.Create; //������ �����
  jpeg:=tjpegimage.create; //������ ����
  f:=false; // �������������� ����
  seq:=1;
end;

function encode(text:string):string;
var
  Encoder: TIdEncoderMIME;
begin
  Encoder := TIdEncoderMIME.Create(nil);
  try
    result := Encoder.EncodeString(Text);
  finally
    FreeAndNil(Encoder);
  end
end;

function decode(text:string):string;
var
  decoder: TIdDecoderMIME;
begin
  decoder := TIdDecoderMIME.Create(nil);
  try
    result := Decoder.DecodeString(Text);
  finally
    FreeAndNil(Decoder);
  end
end;

procedure send(text:string);
begin
  Form1.ClientSocket1.Socket.SendText(text);
  Wait := True;
  log('>'+text);
  seq:=seq+1;
end;

procedure Tform1.waiting;
var
  i:integer;
begin
  i:=10;
  while (wait) or (i>0) do
  begin
    sleep(100);
    application.processmessages;
    i:=i-1;
  end;
end;

procedure TForm1.StartButtonClick(Sender: TObject);
var
  ip:string;
begin
  ip := '172.22.2.9';
  send('OPTIONS rtsp://'+ip+'/user=admin&password=3325&channel=1&stream=0.sdp RTSP/1.0'#13#10'CSeq: 2'#13#10'User-Agent: LibVLC/2.1.5'#13#10#13#10);
  //  RTSP/1.0 200 OK
  //  Server: H264DVR 1.0
  //  cseq: 2
  //  Public: OPTIONS, DESCRIBE, SETUP, TEARDOWN, GET_PARAMETER, PLAY, PAUSE
  waiting;
  send('DESCRIBE rtsp://'+ip+'/user=admin&password=3325&channel=1&stream=0.sdp RTSP/1.0'#13#10'CSeq: 3'#13#10'User-Agent: LibVLC/2.1.5'#13#10'Accept: application/sdp'#13#10#13#10);
  //  RTSP/1.0 200 OK
  //  Content-Type: application/sdp
  //  Server: H264DVR 1.0
  //  cseq: 3
  //  Content-Base: rtsp://172.22.2.9/user=admin&password=3325&channel=1&stream=0.sdp
  //  Cache-Control: private
  //  x-Accept-Retransmit: our-retransmit
  //  x-Accept-Dynamic-Rate: 1
  //  Content-Length: 372
  //
  //  v=0
  //  o=- 38990265062388 38990265062388 IN IP4 172.22.2.9
  //  s=RTSP Session
  //  c=IN IP4 172.22.2.9
  //  t=0 0
  //  a=control:*
  //  a=range:npt=0-
  //  m=video 0 RTP/AVP 96
  //  a=rtpmap:96 H264/90000
  //  a=range:npt=0-
  //  a=framerate:0S
  //  a=fmtp:96 profile-level-id=64001f; packetization-mode=1; sprop-parameter-sets=Z2QAH62EAQwgCGEAQwgCGEAQwgCEK1B0CTI=,aO48sA==
  //  a=framerate:25
  //  a=control:trackID=3
  waiting;
  send('SETUP rtsp://'+ip+'/user=admin&password=3325&channel=1&stream=0.sdp/trackID=3 RTSP/1.0'#13#10'CSeq: 4'#13#10'User-Agent: LibVLC/2.1.5'#13#10'Transport: RTP/AVP;unicast;client_port=54700-54701'#13#10#13#10);
  //RTSP/1.0 200 OK
  //Server: H264DVR 1.0
  //cseq: 4
  //Session: 102222730;timeout=60
  //Transport: RTP/AVP;unicast;mode=PLAY;source=172.22.2.9;client_port=54700-54701;server_port=40002-40003;ssrc=00001BCC
  //Cache-Control: private
  //x-Dynamic-Rate: 1

  waiting;
  send('PLAY rtsp://'+ip+'/user=admin&password=3325&channel=1&stream=0.sdp RTSP/1.0'#13#10'CSeq: 5'#13#10'User-Agent: LibVLC/2.1.5'#13#10'Session: '+session+#13#10'Range: npt=0.000-'#13#10#13#10);
  // ������ 54700 ?

  IdUDPServer1.Active := True;
  // ����� ��� � 55 ���
  waiting;
  send('GET_PARAMETER rtsp://'+ip+'/user=admin&password=3325&channel=1&stream=0.sdp RTSP/1.0'#13#10'CSeq: 6'#13#10'User-Agent: LibVLC/2.1.5'#13#10'Session: '+session+#13#10#13#10);

  // ������ ����� ����=54700  h264
  //SPS Sequence parameter set(80) Seq=1
  //PPS Picture parameter set (60) Seq=2
  //SEI(60) Seq=3
  //FU-A Start:IDR-Slice(1450)  Seq=4
  //FU-A(1450) Seq=5
  //FU-A(1450) Seq=.....
  //FU-A End(807) Seq=61
  //FU-A Start:non-IDR-Slice(1450) Seq=62
  //FU-A(1450) Seq=63
  //FU-A(1450) Seq=64
  //FU-A End(807) Seq=65

  //waiting;
  //send('TEARDOWN rtsp://172.22.2.9/user=admin&password=3325&channel=1&stream=0.sdp RTSP/1.0'#13#10'CSeq: 7'#13#10'User-Agent: LibVLC/2.1.5'#13#10'Session: 88086920'#13#10#13#10);
  //IdUDPServer1.Active := false;;


end;

procedure TForm1.ClientSocket1Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  s:string;
//  start:Integer;
  pkg:Tpkg;
  data: string;
  ps : Integer;
begin
  Application.ProcessMessages; //��������� ��������� ��������� (��� ���������� ���������)
  s:=socket.ReceiveText; //�������� ������ ������
  if pos('RTSP/1.0 200 OK', s) > 0 then
    try
      pkg := Tpkg.create(s);
      if pkg.get('session') > '' then
        session := pkg.get('session');
      if pkg.get('sprop-parameter-sets') > '' then  // sps-pps in RTSP
      begin
        data := pkg.get('sprop-parameter-sets');
        ps := pos(',', data);
        sps := decode(copy(data,1, ps-1));   // bytes
        pps := decode(Copy(data, ps+1, Length(data)));  // bytes
      end;
    finally
      FreeAndNil(pkg);
    end;

  log('<'+s);

  wait := false;
 { start:=pos('����',s); //����� ������ ����� JPEG � ������ (FFD8)
  if start>0 then
  begin
    if mem.Size>0 then
    Begin
      mem.Position := 0;
      jpeg.LoadFromStream(mem);
      mem.Clear;
      Image1.Canvas.Lock;
      try
        Image1.Picture.Bitmap.assign(JPEG); //���������� ���� �� image ��� �����������
      finally
        Image1.Canvas.Unlock;
      end;
    end;
    delete(s,1,start-1); //����� �����������
    mem.Write(s[1],length(s)); //����� ����� ������ � �����
    f:=true;
  end
  else
   if f then
     mem.Write(s[1],length(s));   }
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ClientSocket1.Close;
end;

function toString(data:TIdBytes):string;
begin

end;

function intToHex(Value: integer): string;
var
  stb, mlb: integer;
const
  hex = '0123456789ABCDEF';
begin
  stb := Value div 16;
  mlb := Value - 16 * stb;
  result := hex[stb + 1] + hex[mlb + 1];
end;

procedure TForm1.IdUDPServer1UDPRead(AThread: TIdUDPListenerThread;
  AData: TIdBytes; ABinding: TIdSocketHandle);
var
  i:Integer;
  s:string;
  last:Integer;
begin
  last := Length(s);
  SetLength(raw, Length(raw)+length(s));
  for i:=0 to length(AData) do
  begin
    s:=s+inttohex(adata[i])+' ';
    raw[last+i] := adata[i];
  end;
  log('');
  log(s);
  //
end;

procedure TForm1.StopButtonClick(Sender: TObject);
begin
  send('TEARDOWN rtsp://172.22.2.9/user=admin&password=3325&channel=1&stream=0.sdp RTSP/1.0'#13#10'CSeq: 7'#13#10'User-Agent: LibVLC/2.1.5'#13#10'Session: 88086920'#13#10#13#10);
  Memo1.Lines.SaveToFile('captured.txt');
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  memo1.Clear;
end;

end.
