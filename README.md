# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a demo project showing the work with RTSP H.264 cameras.
Capabilities:
- sending commands to the camera
- receiving a camera response
- RTP stream capture
- H.264 decoding
- display of the received frame

### How do I get set up? ###

Requirements:
- Delphi 7
- indy 10
- ffmpeg
