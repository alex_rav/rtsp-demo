unit ffmpeg;

interface

uses
  ShellAPI, Forms, SysUtils, Windows,
  rtp_types, rtp;

type
  TFFMpeg = class
  private
  public
    procedure decode(pkg:TRTP);
  end;

implementation

procedure TFFMpeg.decode(pkg:TRTP);
begin
  ShellExecute(0, 'open', pchar(extractfilepath(Application.exename)+'ffmpeg.exe'), '-i frame.raw -f image2 -y myframe.jpg', '', SW_SHOW);
end;

end.
