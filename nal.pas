unit nal;

interface

const FU_A = 28;
const SPS = 7;
const PPS = 8;

type
  TNAL = class
  private
    nal:byte;
  public
    procedure setnal(nal: byte);
    function getF():Byte;
    function getNRI():Byte;
    function getType():Byte;
    procedure print();

  end;


implementation


procedure TNAL.setnal(nal: byte);
begin
    self.nal := nal;
end;

function TNAL.getF():Byte;
begin
    Result := ((nal shr 8) and $1);
end;

function TNAL.getNRI():Byte;
begin
    Result := ((nal shr 5) and $3);
end;

function TNAL.getType():Byte;
begin
    Result := (nal and $1f);
end;

procedure TNAL.print();
begin
    writeln('|f|NRI type|');
    writeln('|%d|%2d|%5d|', getF(), getNRI(), getType());
end;

end.
