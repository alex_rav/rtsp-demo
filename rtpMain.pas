unit rtpMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ScktComp, StdCtrls, jpeg, IdBaseComponent, IdCoder, IdCoder3to4,
  IdCoderMIME, ExtCtrls, IdComponent, IdUDPBase, IdUDPServer,
  IdGlobal, IdSocketHandle, Buttons, IdTCPConnection, IdTCPClient,
  rtp_types, dxGDIPlusClasses;

type
  TrtpForm = class(TForm)
    Image1: TImage;
    Memo1: TMemo;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    CheckBox1: TCheckBox;
    DecoderGroup: TRadioGroup;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Edit2: TEdit;
    StartButton: TButton;
    StopButton: TButton;
    Bevel1: TBevel;
    procedure StartButtonClick(Sender: TObject);
    procedure StopButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure rtcpRead(response: string);
    procedure rtpRead(AData: TData);
    procedure rtpFrame(raw: TData);
    procedure SaveRaw(filename:string; raw:TData);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rtpForm: TrtpForm;

implementation

uses
  rtp, H264, rtspclient, ffmpeg;

var
  oneFrame:Boolean;
  frameReady:Boolean;
  client:TrtspClient;

{$R *.dfm}

procedure log(text:string);
begin
  rtpform.memo1.lines.add(text);
end;

procedure TrtpForm.SaveRaw(filename:string; raw:TData);
var
  f: file of Byte;
  i: Integer;
begin
  AssignFile(f, filename);
  Rewrite(f);
  for i:=0 to Length(raw)-1 do
    write(f, raw[i]);
  CloseFile(f);
end;

procedure TrtpForm.rtcpRead(response: string);
begin
  log('<'+response);
end;

procedure TrtpForm.rtpRead(AData: TData);
var
  pkg: TRTP;
begin
  pkg := TRTP.create(AData);
  log('len='+IntToStr(Length(AData))+' seq='+inttostr(pkg.getSequence)+' '+pkg.getTypeText());
  pkg.Free;
end;

procedure TrtpForm.rtpFrame(raw: TData);
var
  rtp: TRTP;
  decoder: TFFmpeg;
  i: integer;
const
  img = 'myframe.jpg';
begin
  rtp := TRTP.create(raw);
  try
    if DecoderGroup.ItemIndex = 0 then
    begin
      SaveRaw('frame.raw', raw);
      log('decode h264');
      DeleteFile(img);
      decoder := TFFMpeg.Create;
      decoder.decode(rtp);
      i:=0;
      while (not FileExists(img))or(i<20) do
      begin
        Sleep(100);
        Application.ProcessMessages;
        i:=i+1;
      end;
      if FileExists(img) then
         Image1.Picture.LoadFromFile(img);
      Decoder.Free;
    end;
  finally
   FreeAndNil(rtp);
  end;
  if oneFrame then
  begin
    client.send('TEARDOWN');
    client.close;
  end;
end;

procedure TrtpForm.StartButtonClick(Sender: TObject);
begin
    Client := TrtspClient.create(edit1.text, strtoint(edit2.text));
    Client.OnRtcpReadEnd := rtcpRead;
    Client.OnRtpRead := rtpRead;
    Client.OnRtpFrame := rtpFrame;
    Client.Open;

  client.oneFrame := CheckBox1.Checked;
  frameReady := false;
  client.username := 'admin';
  client.password := '3325';
  client.send('OPTIONS');
  log('>OPTIONS');
  //send('OPTIONS rtsp://'+ip+'/user=admin&password=3325&channel=1&stream=0.sdp RTSP/1.0'#13#10'CSeq: 2'#13#10'User-Agent: LibVLC/2.1.5'#13#10#13#10);
  //  RTSP/1.0 200 OK
  //  Server: H264DVR 1.0
  //  cseq: 2
  //  Public: OPTIONS, DESCRIBE, SETUP, TEARDOWN, GET_PARAMETER, PLAY, PAUSE
  //waiting;
  client.channel := 1;
  client.stream := 0;
  client.send('DESCRIBE');
  log('>DESCRIBE');
  //send('DESCRIBE rtsp://'+ip+'/user=admin&password=3325&channel=1&stream=0.sdp RTSP/1.0'#13#10'CSeq: 3'#13#10'User-Agent: LibVLC/2.1.5'#13#10'Accept: application/sdp'#13#10#13#10);
  //  RTSP/1.0 200 OK
  //  Content-Type: application/sdp
  //  Server: H264DVR 1.0
  //  cseq: 3
  //  Content-Base: rtsp://172.22.2.9/user=admin&password=3325&channel=1&stream=0.sdp
  //  Cache-Control: private
  //  x-Accept-Retransmit: our-retransmit
  //  x-Accept-Dynamic-Rate: 1
  //  Content-Length: 372
  //
  //  v=0
  //  o=- 38990265062388 38990265062388 IN IP4 172.22.2.9
  //  s=RTSP Session
  //  c=IN IP4 172.22.2.9
  //  t=0 0
  //  a=control:*
  //  a=range:npt=0-
  //  m=video 0 RTP/AVP 96
  //  a=rtpmap:96 H264/90000
  //  a=range:npt=0-
  //  a=framerate:0S
  //  a=fmtp:96 profile-level-id=64001f; packetization-mode=1; sprop-parameter-sets=Z2QAH62EAQwgCGEAQwgCGEAQwgCEK1B0CTI=,aO48sA==
  //  a=framerate:25
  //  a=control:trackID=3
  //waiting;
  client.trackID := 3;
  client.send('SETUP');
  log('>SETUP');
  //send('SETUP rtsp://'+ip+'/user=admin&password=3325&channel=1&stream=0.sdp/trackID=3 RTSP/1.0'#13#10'CSeq: 4'#13#10'User-Agent: LibVLC/2.1.5'#13#10'Transport: RTP/AVP;unicast;client_port=54700-54701'#13#10#13#10);
  //RTSP/1.0 200 OK
  //Server: H264DVR 1.0
  //cseq: 4
  //Session: 102222730;timeout=60
  //Transport: RTP/AVP;unicast;mode=PLAY;source=172.22.2.9;client_port=54700-54701;server_port=40002-40003;ssrc=00001BCC
  //Cache-Control: private
  //x-Dynamic-Rate: 1

  //waiting;
  client.send('PLAY');
  log('>PLAY');
  //send('PLAY rtsp://'+ip+'/user=admin&password=3325&channel=1&stream=0.sdp RTSP/1.0'#13#10'CSeq: 5'#13#10'User-Agent: LibVLC/2.1.5'#13#10'Session: '+session+#13#10'Range: npt=0.000-'#13#10#13#10);
  // read udp port 54700
  client.Rtp.DefaultPort := 54700;
  client.Rtp.Active := True;
  // send every 55 sek
  //waiting;
  client.send('GET_PARAMETER');
  log('>GET_PARAMETER');
  //send('GET_PARAMETER rtsp://'+ip+'/user=admin&password=3325&channel=1&stream=0.sdp RTSP/1.0'#13#10'CSeq: 6'#13#10'User-Agent: LibVLC/2.1.5'#13#10'Session: '+session+#13#10#13#10);

  // udp port=54700  h264 stream
  //SPS Sequence parameter set Seq=1
  //PPS Picture parameter set Seq=2
  //SEI Seq=3
  //FU-A Start:IDR-Slice(1450)  Seq=4
  //FU-A(1450) Seq=5
  //FU-A(1450) Seq=.....
  //FU-A End(807) Seq=61
  //FU-A Start:non-IDR-Slice(1450) Seq=62
  //FU-A(1450) Seq=63
  //FU-A(1450) Seq=64
  //FU-A End(807) Seq=65

  //.... stop
  //send('TEARDOWN rtsp://172.22.2.9/user=admin&password=3325&channel=1&stream=0.sdp RTSP/1.0'#13#10'CSeq: 7'#13#10'User-Agent: LibVLC/2.1.5'#13#10'Session: 88086920'#13#10#13#10);
end;

procedure TrtpForm.StopButtonClick(Sender: TObject);
begin
  if assigned(client) then
  begin
    client.send('TEARDOWN');
    client.close;
    FreeAndNil(client);
  end;
end;

procedure TrtpForm.FormCreate(Sender: TObject);
begin
  memo1.Clear;
end;

end.
