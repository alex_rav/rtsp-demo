unit rtp;

interface

uses rtp_types;

//size of the RTP header:
const HEADER_SIZE = 12;

type
  TRTP = class
    private
      data:TData;
      //sps:TData;
      //pps:TData;
      //sei:TData;
    public
      Profile_idc: Byte;
      Constraint_set0_flag: Byte;
      Constraint_set1_flag: Byte;
      Constraint_set2_flag: Byte;
      Constraint_set3_flag: Byte;
      Constraint_set4_flag: Byte;
      Constraint_set5_flag: Byte;
      Reserved_zero_2bits: Byte;
      Level_id: Byte; //31 [Level 3.1 14 Mb/s]
      seq_parameter_set_id: Byte;
      chroma_format_id: Byte;
      bit_depth_luma_minus8: Byte;
      bit_depth_chroma_minus8: Byte;
      qpprime_y_zero_transform_bypass_flag: Byte;
      seq_scaling_matrix_present_flag: Byte;

      constructor create(data:TData);
      destructor destroy(); override;
      procedure setData(data:TData);
      //procedure setSPS(data:TData);
      //procedure setPPS(data:TData);
      //procedure setSEI(data:TData);

      function getData():TData;
      function getPayLoad():TData;
      function getPayloadType():Byte;
      function isMarker():Boolean;
      function getSequence():integer;
      function getTimeStamp():integer;
      function getNFbit():Byte;
      function getNRI():Byte;

      function isSPS():Boolean;
      function isPPS():Boolean;
      function isSEI():Boolean;
      function isIDR():Boolean;

      procedure parseSEI();
      procedure parseSPS();
      procedure parsePPS();

      function getSPS():TData;
      function getPPS():TData;
      function getSEI():Tdata;
      function getIDR():Tdata;

      function getSSI():integer;
      function getFUType():Byte;
      function isFUFirst():Boolean;
      function isFUEnd():Boolean;
      function isFU():boolean;
      function isFuIdr():Boolean;
      function isFuNonIdr():Boolean;

      procedure print();
      function getTypeText():string;
  end;

implementation

constructor TRTP.create(data:TData);
var
  i:Integer;
begin
  SetLength(Self.data, Length(data));
  for i:=0 to Length(data)-1 do
    self.data[i] := Data[i];
end;

destructor TRTP.destroy();
begin
  setlength(data, 0);
end;

procedure TRTP.setData(data:TData);
var
  i:Integer;
begin
  SetLength(Self.data, Length(data));
  for i:=0 to Length(data)-1 do
    self.data[i] := Data[i];
end;
{
procedure TRTP.setSPS(data:TData);
var
  i:Integer;
begin
  SetLength(sps, Length(data));
  for i:=0 to Length(data)-1 do
    sps[i] := Data[i];
end;

procedure TRTP.setPPS(data:TData);
var
  i:Integer;
begin
  SetLength(pps, Length(data));
  for i:=0 to Length(data)-1 do
    pps[i] := Data[i];
end;

procedure TRTP.setSEI(data:TData);
var
  i:Integer;
begin
  SetLength(sei, Length(data));
  for i:=0 to Length(data)-1 do
    sei[i] := Data[i];
end;
}
function TRTP.getData():TData;
begin
  result := data;
end;

function TRTP.getPayLoad():TData;
var
  tmp: TData;
  i: Integer;
begin
  SetLength(tmp, length(data)-header_size-2);
  for i:= HEADER_SIZE+2 to Length(data)-1 do
    tmp[i-HEADER_SIZE-2] := data[i];
  result := tmp;
end;

function TRTP.isMarker():Boolean;
var
  f: Integer;
begin
    f := (Data[1] and $80);
    Result := (f <> 0);
end;

function TRTP.getPayloadType():Byte;
begin
    result := (data[1] and $7f);
end;

function TRTP.getSequence():integer;
begin
    result := 256*data[2] + data[3];
end;

function TRTP.getTimeStamp():integer;
begin
    result := $10000*data[4] + $1000*data[5] + $100*data[6] + data[7];
end;


// Synchronization Source identifier
function TRTP.getSSI():integer;
begin
    result := $10000*data[8] + $1000*data[9] + $100*data[10] + data[11];
end;

// 0... .... = F bit: No bit errors or other syntax violations
function TRTP.getNFbit():Byte;
var
  f: Integer;
begin
  if isMarker then
  begin
    f := (data[HEADER_SIZE] and $80);
    if f <> 0 then
      Result := 1
    else
      Result := 0;
  end
  else
    result := 0;
end;


// .11. .... = Nal_ref_idc (NRI)
function TRTP.getNRI():Byte;
begin
  if isMarker then
  begin
    result := (data[HEADER_SIZE] and $60) shr 5;
  end
  else
    result := 0;
end;

// ...0 0111 = Type: NAL unit - Sequence parameter set (7)
function TRTP.isSPS():Boolean;
begin
  result := (isMarker) and ((data[HEADER_SIZE] and $1f) = $07);
end;

procedure TRTP.parseSPS();
begin
  //0110 0100
  Profile_idc := data[header_size] and $64;
  //0... .... =
  Constraint_set0_flag := data[header_size] and $80;
  //.0.. .... =
  Constraint_set1_flag := data[header_size] and $40;
  //..0. .... =
  Constraint_set2_flag := data[header_size] and $20;
  //...0 .... =
  Constraint_set3_flag := data[header_size] and $10;
  //.... 0... =
  Constraint_set4_flag := data[header_size] and $08;
  //.... .0.. =
  Constraint_set5_flag := data[header_size] and $04;
  //.... ..00 =
  Reserved_zero_2bits  := data[header_size] and $03;
  //0001 1111 =
  Level_id := data[header_size+1] and $1f;
  //1... .... =
  seq_parameter_set_id := data[header_size+2] and $80;
  //.010 .... =
  chroma_format_id := data[header_size+2] and $20 shr 5;
  //.... 1... =
  bit_depth_luma_minus8 := data[header_size+2] and $08;
  //.... .1.. =
  bit_depth_chroma_minus8 := data[header_size+2] and $04;
  //.... ..0. =
  qpprime_y_zero_transform_bypass_flag := data[header_size+2] and $02;
  //.... ...1 =
  seq_scaling_matrix_present_flag := data[header_size+2] and $01;
end;

//H264 NAL Unit Payload
function TRTP.getSPS():Tdata;
var
  tmp: TData;
  i: integer;
begin
  if isSPS() then
  begin
    // raw data
    SetLength(tmp, length(data)-header_size);
    for i:=header_size to length(data)-1 do
      tmp[i-header_size] := data[i];
    Result := tmp;
  end
  else
  begin
    SetLength(tmp, 0);
    result := tmp;
  end;
end;

function TRTP.isIDR():Boolean;
begin
  result := (isMarker) and ((data[HEADER_SIZE] and $1f) = $01);
end;

function TRTP.getIDR():Tdata;
var
  tmp: TData;
  i: integer;
begin
  if isIDR then
  begin
    // raw data
    SetLength(tmp, length(data)-header_size);
    for i:=header_size to length(data)-1 do
      tmp[i-header_size] := data[i];
    Result := tmp;
  end
  else
  begin
    SetLength(tmp, 0);
    result := tmp;
  end;
end;


// ...0 0110 = Type: NAL unit - Supplemental enhancement information (SEI) (6)
function TRTP.isSEI():Boolean;
begin
  result := (isMarker) and ((data[HEADER_SIZE] and $1f) = $06);
end;

procedure TRTP.parseSEI();
begin
  //payloadType: Unknown (229)
  //PayloadSize: 1
  //1... .... = rbsp_stop_bit: 1
  //.111 1000 = rbsp_trailing_bits: 120
end;

//H264 NAL Unit Payload
function TRTP.getSEI():Tdata;
var
  tmp: TData;
  i: integer;
begin
  if isSEI then
  begin
    // raw data
    SetLength(tmp, length(data)-header_size);
    for i:=header_size to length(data)-1 do
      tmp[i-header_size] := data[i];
    Result := tmp;
  end
  else
  begin
    SetLength(tmp, 0);
    result := tmp;
  end;
end;


// ...0 1000 = Type: NAL unit - Picture parameter set (8)
function TRTP.isPPS():Boolean;
begin
  result := (isMarker) and ((data[HEADER_SIZE] and $1f) = $08);
end;

procedure TRTP.parsePPS();
begin
      //1... .... = pic_parameter_set_id: 0
      //.1.. .... = seq_parameter_set_id: 0
      //..1. .... = entropy_coding_mode_flag: 1
      //...0 .... = pic_order_present_flag: 0
      //.... 1... = num_slice_groups_minus1: 0
      //.... .1.. = num_ref_idx_l0_active_minus1: 0
      //.... ..1. = num_ref_idx_l1_active_minus1: 0
      //.... ...0 = weighted_pred_flag: 0
      //00.. .... = weighted_bipred_idc: 0
      //..1. .... = pic_init_qp_minus26: 0
      //...1 .... = pic_init_qs_minus26: 0
      //.... 1... = chroma_qp_index_offset: 0
      //.... .1.. = deblocking_filter_control_present_flag: 1
      //.... ..0. = constrained_intra_pred_flag: 0
      //.... ...0 = redundant_pic_cnt_present_flag: 0
      //1... .... = transform_8x8_mode_flag: 1
      //.0.. .... = pic_scaling_matrix_present_flag: 0
      //..1. .... = second_chroma_qp_index_offset: 0
      //...1 .... = rbsp_stop_bit: 1
      //.... 0000 = rbsp_trailing_bits: 0
      // data[header_size] and $64;
end;

// ...0 1000 = Type: NAL unit - Picture parameter set (8)
function TRTP.getPPS():TData;
var
  tmp: TData;
  i: integer;
begin
  if isPPS then
  begin
    // raw data
    SetLength(tmp, length(data)-header_size);
    for i:=header_size to length(data)-1 do
      tmp[i-header_size] := data[i];
    Result := tmp;
  end
  else
  begin
    SetLength(tmp, 0);
    result := tmp;
  end;
end;

function TRTP.isFU():boolean;
begin
  result := (getFuType() = 28);
end;

function TRTP.getFuType():Byte;
begin
    result := (data[HEADER_SIZE] and $1f);
end;

function TRTP.isFuFirst():Boolean;
var
  f: Integer;
begin
    f := (data[HEADER_SIZE+1] and $80);
    Result := (not isMarker)and(f <> 0);  // not marker!
end;

function TRTP.isFuEnd():Boolean;
var
  f: Integer;
begin
    f := (data[HEADER_SIZE+1] and $40);
    Result := (isMarker)and(f <> 0); // marker!
end;

function TRTP.isFuIdr():Boolean;
var
  f: Integer;
begin
    f := (data[HEADER_SIZE+1] and $1f);
    Result := (not isMarker)and(f = 5);
end;

function TRTP.isFuNonIdr():Boolean;
var
  f: Integer;
begin
    f := (data[HEADER_SIZE+1] and $1f);
    Result := (not isMarker)and(f = 1);
end;

function TRTP.getTypeText():string;
begin
  if isMarker then
    Result := 'Mark';
  if isSPS then
    Result := Result + ' SPS';
  if isPPS then
    Result := Result + ' PPS';
  if isSEI then
    Result := Result + ' SEI';
  if isFU then
    Result := Result + ' FU-A';
  if (isFU)and(isFUFirst) then
    Result := Result + ' Start';
  if (isFU)and(isFUEnd) then
    Result := Result + ' End';
  if isFU then
  begin
    if isFuIDR then
      Result := Result + ' IDR-Slice'
    else if isFuNonIdr then
      Result := Result + ' non IDR-Slice';
  end;
end;


procedure TRTP.print();
begin
end;

end.
